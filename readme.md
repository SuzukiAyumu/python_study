# setup task

1. install python

    https://www.python.org/

1. install kivy

    https://kivy.org/#download

    seelct at installation at windows

    https://kivy.org/docs/installation/installation-windows.html

    1. install setuptools

        ```
        python -m pip install --upgrade pip wheel setuptools
        ```
    
    2. Install the dependencies (skip gstreamer (~120MB) if not needed, see Kivy’s dependencies):

        ```
        python -m pip install docutils pygments pypiwin32 kivy.deps.sdl2 kivy.deps.glew
        python -m pip install kivy.deps.gstreamer
        ``` 

        For Python 3.5 only we additionally offer angle which can be used instead of glew and can be installed with:

        ```
        python -m pip install kivy.deps.angle
        ```
    
    1. Install kivy:

        ```
        python -m pip install kivy
        ```
    
    1. (Optionally) Install the kivy examples:
        
        The examples are installed in the share directory under the root directory where python is installed.

        ```
        python -m pip install kivy_examples
        ```

        That’s it. You should now be able to import kivy in python or run a basic example if you installed the kivy examples:

        ```
        python share\kivy-examples\demo\showcase\main.p 
        ```